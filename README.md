## 汇溪和他们的小伙伴们

> **汇溪**： 汇溪成海之意，象征汇聚多方力量。



感谢大家的支持😉。没想到有这么多的人愿意加入，真是受宠若惊呀😆。很多的东西都没有规划好，毕竟第一次（多多见谅）。

工作经验尚浅，许多东西不懂，劳烦各位了😜 （任务的计算和划分需要大量时间，等**周末**）





## 补充事项

1.记得在IDE上下载阿里巴巴的编码检测	Alibaba Java Coding Guidelines，**力求规范**

2.小程序的Git地址：https://gitee.com/TheOldMan/weiju-wechat.git

3.小程序后台的Git地址：https://gitee.com/huixi_and_their_friends/weiju.git





## 项目注意事项

> 后端项目用的是： https://gitee.com/bweird/lenosp.git



1. 微信小程序的配置

   ```yaml
   在 application.yml 文件中（在IDEA 中双击 shift搜索）
   
   weiJu:
     appId:
     appSecret:
     
     填上你申请小程序的 appid 和 appSecret 
   ```

2. 数据库 （mysql）

   ```
   数据库ip: 47.106.225.52
   用户名： root
   密码： weiju
   
   搭建的一个测试库随便搞
   ```

3. 存储

   > 文件的存储用的是阿里的OSS 。 一系列配置都在文件中有。 我分派的是一个子账号，权限只能操作OSS，不用担心安全问题。

   

