package com.len.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Table(name = "sys_version")
@Getter
@Setter
@ToString
public class SysVersion implements Serializable {
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "v_no")
    private String vNo;

    @Column(name = "v_desc")
    private String vDesc;

    @Column(name = "h5_desc")
    private String h5Desc;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8:00")
    @Column(name = "create_time")
    private Date createTime;

}