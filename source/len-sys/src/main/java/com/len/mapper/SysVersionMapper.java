package com.len.mapper;

import com.len.base.BaseMapper;
import com.len.dto.ListVersionDTO;
import com.len.entity.SysVersion;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysVersionMapper extends BaseMapper<SysVersion, String> {

    SysVersion getVersionByNo(@Param("vNo") String vNo);

    List<ListVersionDTO> listVersion();
}