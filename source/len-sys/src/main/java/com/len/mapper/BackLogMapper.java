package com.len.mapper;

import com.len.base.BaseMapper;
import com.len.entity.BackLog;

public interface BackLogMapper extends BaseMapper<BackLog,Integer> {
}