<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>贷贷连 | 统计报表</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="/static/ext/adminlte/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/static/ext/select2/css/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/static/ext/adminlte/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/static/ext/adminlte/dist/css/skins/_all-skins.css">
    <link rel="stylesheet" href="/static/ext/toastr/toastr.min.css">
    <link rel="stylesheet" href="/static/ext/btnloading/dist/ladda-themeless.min.css">
    <link rel="stylesheet" href="/static/admin/css/onebase.css">
    <link rel="stylesheet" type="text/css" href="/static/ext/remodal/remodal.css" media="all">
    <link rel="stylesheet" type="text/css" href="/static/ext/remodal/remodal-default-theme.css" media="all">
    <!-- jQuery 2.2.3 -->
    <script src="/static/ext/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="/static/ext/adminlte/bootstrap/js/bootstrap.min.js"></script>
    <script src="/static/admin/js/init.js"></script>

    <link rel="stylesheet" href="/static/ext/adminlte/dist/css/font-awesome.min.css">
    <link rel="stylesheet" href="/static/ext/adminlte/dist/css/ionicons.min.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="fakeloader"></div>
<link href="/static/ext/fakeloader/css/fakeLoader.css" rel="stylesheet">
<script src="/static/ext/fakeloader/js/fakeLoader.min.js"></script>
<script type="text/javascript">

    $(".fakeloader").fakeLoader({
        timeToHide: 99999,
        bgColor: "rgba(52, 52, 52, .5)",
        spinner: "spinner7"
    });

    $('.fakeloader').hide();
</script>
<script type="text/javascript">$('.fakeloader').show();</script>
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="/index/index.html" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>DDL</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><img src="/static/admin/img/ddl_white.png" width="120px"</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">导航开关</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown messages-menu">

                        <!--            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                      <i class="fa fa-envelope-o"></i>
                                      <span class="label label-success">4</span>
                                    </a>-->

                        <ul class="dropdown-menu">
                            <li class="header">您有4个消息</li>

                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    <li><!-- start message -->
                                        <a href="#">
                                            <div class="pull-left">
                                                <img src="/static/ext/adminlte/dist/img/avatar5.png" class="img-circle"
                                                     alt="User Image">
                                            </div>
                                            <h4>
                                                张三
                                                <small><i class="fa fa-clock-o"></i> 5 分钟前</small>
                                            </h4>
                                            <p>吃饭了吗？</p>
                                        </a>
                                    </li>
                                    <!-- end message -->
                                    <li>
                                        <a href="#">
                                            <div class="pull-left">
                                                <img src="/static/ext/adminlte/dist/img/avatar5.png" class="img-circle"
                                                     alt="User Image">
                                            </div>
                                            <h4>
                                                李四
                                                <small><i class="fa fa-clock-o"></i> 2 小时前</small>
                                            </h4>
                                            <p>麻烦发下今天的文章哦。</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer"><a href="#">查看所有消息</a></li>
                        </ul>
                    </li>
                    <!-- Notifications: style can be found in dropdown.less -->
                    <li class="dropdown notifications-menu">
                        <!--            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                      <i class="fa fa-bell-o"></i>
                                      <span class="label label-warning">10</span>
                                    </a>-->
                        <ul class="dropdown-menu">
                            <li class="header">您有10个通知</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-users text-aqua"></i> 今天有5个新成员加入
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-warning text-yellow"></i> 这是一条系统警告通知。
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-shopping-cart text-green"></i> 销售了25个产品喔
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-user text-red"></i> 用户名修改通知
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer"><a href="#">查看所有通知</a></li>
                        </ul>
                    </li>
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="/static/ext/adminlte/dist/img/avatar5.png" class="user-image" alt="User Image">
                            <span class="hidden-xs">admin01</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="/static/ext/adminlte/dist/img/avatar5.png" class="img-circle"
                                     alt="User Image">

                                <p>
                                    admin01
                                    <small>上次登录时间：2019-04-17 18:37:55</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <!--              <li class="user-body">
                                            <div class="row">
                                              <div class="col-xs-4 text-center">
                                                <a href="#">个人中心</a>
                                              </div>
                                              <div class="col-xs-4 text-center">
                                                <a href="#">修改头像</a>
                                              </div>
                                              <div class="col-xs-4 text-center">
                                                <a href="#">修改密码</a>
                                              </div>
                                            </div>
                                             /.row
                                          </li>-->
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="/ddl_account/editpassword.html" class="btn btn-default btn-flat">密码修改</a>
                                    <!--                   <a href="#" class="btn btn-default btn-flat clear_cache" url="/login/clearcache.html">清理缓存</a>
                                     -->

                                </div>
                                <div class="pull-right">
                                    <a class="btn btn-default btn-flat logout" url="/login/logout.html">安全退出</a>
                                </div>
                            </li>
                        </ul>
                    </li>

                    <!-- 控制栏切换按钮 -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- 左侧导航栏 -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/static/ext/adminlte/dist/img/avatar5.png" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>admin01</p>
                    2019-04-17 18:37:55          <!--<a href="#"><i class="fa fa-circle text-success"></i> 在线状态</a>-->
                </div>
            </div>


            <!-- search form -->
            <!--      <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                      <input type="text" name="q" class="form-control" placeholder="请输入搜索内容...">
                          <span class="input-group-btn">
                            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                            </button>
                          </span>
                    </div>
                  </form>-->
            <!-- /.search form -->

            <!-- 左侧菜单 -->
            <ul class="sidebar-menu">
                <li menu_id='1'><a href='/index/index.html'><i class='fa fa-home'></i> <span>系统首页</span></a></li>
                <li menu_id='179'><a href='/merchant/merchantlist.html'><i class='fa fa-building-o'></i>
                        <span>商户管理</span></a></li>
                <li menu_id='225'>
                    <a href='/finance/index.html'><i class='fa fa-yen'></i> <span>财务管理</span>
                        <span class='pull-right-container'>
                                     <i class='fa fa-angle-left pull-right'></i>
                                   </span>
                    </a>
                    <ul class='treeview-menu'>
                        <li menu_id='217'><a href='/ddl_account/zhanghu.html'><i class='fa fa-book'></i>
                                <span>账户管理</span></a></li>
                        <li menu_id='226'><a href='/ddl_account/recharge.html'><i class='fa fa-random'></i>
                                <span>充值管理</span></a></li>
                        <li menu_id='243'><a href='/ddl_account/payment.html'><i class='fa fa-share'></i>
                                <span>出款管理</span></a></li>
                    </ul>
                </li>
                <li class='active'>
                    <a href='/ddl_statement/index.html'><i class='fa fa-list'></i> <span>报表清单</span>
                        <span class='pull-right-container'>
                                     <i class='fa fa-angle-left pull-right'></i>
                                   </span>
                    </a>
                    <ul class='treeview-menu'>
                        <li class='active'><a href='/ddl_statement/statdaily.html'><i class='fa fa-angle-right'></i>
                                <span>统计报表</span></a></li>
                        <li menu_id='235'><a href='/ddl_statement/assignment.html'><i class='fa fa-angle-right'></i>
                                <span>债转清单</span></a></li>
                        <li menu_id='241'><a href='/ddl_statement/proxystatdaily.html'><i class='fa fa-angle-right'></i>
                                <span>代理日统计</span></a></li>
                        <li menu_id='242'><a href='/ddl_statement/proxystatmonthly.html'><i
                                        class='fa fa-angle-right'></i> <span>代理月统计</span></a></li>
                        <li menu_id='246'><a href='/ddl_statement/finance.html'><i class='fa fa-angle-right'></i> <span>财务报表</span></a>
                        </li>
                    </ul>
                </li>
                <li menu_id='220'><a href='/ddl_jiekuan_apply/index.html'><i class='fa fa-list'></i> <span>借款申请单</span></a>
                </li>
                <li menu_id='221'><a href='/ddl_jiekuan_ren/index.html'><i class='fa fa-group'></i>
                        <span>借款申请人</span></a></li>
                <li menu_id='238'><a href='/ddl_proxy/index.html'><i class='fa fa-shopping-bag'></i> <span>代理商管理</span></a>
                </li>
                <li menu_id='16'>
                    <a href='/member/index.html'><i class='fa fa-users'></i> <span>平台操作员管理</span>
                        <span class='pull-right-container'>
                                     <i class='fa fa-angle-left pull-right'></i>
                                   </span>
                    </a>
                    <ul class='treeview-menu'>
                        <li menu_id='17'><a href='/member/memberlist.html'><i class='fa fa-list'></i> <span>操作员列表</span></a>
                        </li>
                        <li menu_id='18'><a href='/member/memberadd.html'><i class='fa fa-user-plus'></i>
                                <span>操作员添加</span></a></li>
                        <li menu_id='27'><a href='/auth/grouplist.html'><i class='fa fa-key'></i> <span>权限管理</span></a>
                        </li>
                    </ul>
                </li>
                <li menu_id='166'>
                    <a href='/maintain/index.html'><i class='fa fa-legal'></i> <span>优化维护</span>
                        <span class='pull-right-container'>
                                     <i class='fa fa-angle-left pull-right'></i>
                                   </span>
                    </a>
                    <ul class='treeview-menu'>
                        <li menu_id='174'><a href='/log/loglist.html'><i class='fa fa-street-view'></i>
                                <span>行为日志</span></a></li>
                    </ul>
                </li>
                <li menu_id='68'>
                    <a href='/config/group.html'><i class='fa fa-wrench'></i> <span>系统管理</span>
                        <span class='pull-right-container'>
                                     <i class='fa fa-angle-left pull-right'></i>
                                   </span>
                    </a>
                    <ul class='treeview-menu'>
                        <li menu_id='75'>
                            <a href='/menu/index.html'><i class='fa fa-th-large'></i> <span>菜单管理</span>
                                <span class='pull-right-container'>
                                     <i class='fa fa-angle-left pull-right'></i>
                                   </span>
                            </a>
                            <ul class='treeview-menu'>
                                <li menu_id='124'><a href='/menu/menulist.html'><i class='fa fa-list'></i>
                                        <span>菜单列表</span></a></li>
                                <li menu_id='125'><a href='/menu/menuadd.html'><i class='fa fa-plus'></i>
                                        <span>菜单添加</span></a></li>
                            </ul>
                        </li>
                        <li menu_id='135'><a href='/trash/trashlist.html'><i class='fa  fa-recycle'></i>
                                <span>回收站</span></a></li>
                        <li menu_id='140'><a href='/service/servicelist.html'><i class='fa fa-server'></i>
                                <span>服务管理</span></a></li>
                        <li menu_id='141'>
                            <a href='/addon/index.html'><i class='fa fa-puzzle-piece'></i> <span>插件管理</span>
                                <span class='pull-right-container'>
                                     <i class='fa fa-angle-left pull-right'></i>
                                   </span>
                            </a>
                            <ul class='treeview-menu'>
                                <li menu_id='142'><a href='/addon/hooklist.html'><i class='fa fa-anchor'></i>
                                        <span>钩子列表</span></a></li>
                                <li menu_id='143'><a href='/addon/addonlist.html'><i class='fa fa-list'></i>
                                        <span>插件列表</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                统计报表 </h1>
            <ol class='breadcrumb'>
                <li><a><i class='fa fa-list'></i> 报表清单</a></li>
                <li><a><i class='fa fa-angle-right'></i> 统计报表</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <form action="/ddl_statement/statdaily.html" method="post" class="form_single">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">搜索</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-remove"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>商户名称</label>
                                <select class="form-control" name="SUserId">
                                    <option value="0">全部</option>

                                    <option value="200018">至弈(200018)</option>

                                    <option value="200019">飞鸟贷(200019)</option>

                                    <option value="200026">现金巴士(200026)</option>

                                    <option value="200027">造艺(200027)</option>

                                    <option value="200008">200008(200008)</option>

                                    <option value="2000081">2000081(2000081)</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>商户类型</label>
                                <select class="form-control" name="merchantType">
                                    <option value="0">全部</option>
                                    <option value="1">普通商户</option>
                                    <option value="2">保理商户</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" style="overflow: hidden;">
                                <label>统计日期</label><br>
                                <div class="col-xs-4" style="padding-left: 0px;"><input name="create_time1"
                                                                                        id="create_time1"
                                                                                        class="form-control" value=""
                                                                                        onfocus="WdatePicker()"
                                                                                        autocomplete="off"></div>
                                <div style="float:left;">-</div>
                                <div class="col-xs-4"><input name="create_time2" id="create_time2" class="form-control"
                                                             value="" onfocus="WdatePicker()" autocomplete="off"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>结算状态</label>
                                <select class="form-control" name="check_status">
                                    <option value="0">全部</option>
                                    <option value="1">未对账</option>
                                    <option value="2">已平账</option>
                                    <option value="3">账不平</option>
                                </select>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary ladda-button" id="search_btn" data-style="slide-up"
                                target-form="form_single">提 交
                        </button>
                        <button type="submit" class="btn btn-primary ladda-button" id="export" data-style="slide-up"
                                target-form="form_single">导 出
                        </button>
                    </div>
                </div>
            </form>

            <div class="box">
                <div class="box-header">
                    <div class="box-tools">收入：【1980.1元】</div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-hover">
                        <thread>
                            <tr>
                                <th width="15%">商户名</th>
                                <th>放款日期</th>
                                <th>日末余额</th>
                                <th>放款笔数</th>
                                <th>放款金额</th>
                                <th>债转手续费</th>
                                <th>代付手续费</th>
                                <th>状态</th>
                            </tr>
                        </thread>
                        <tbody>
                        <tr>
                            <td title="上海两橙信息科技有限公司">造艺</td>
                            <td>20181004</td>
                            <td>620,249.05</td>
                            <td>199</td>
                            <td>199,000.00</td>
                            <td>1,990.00</td>
                            <td>199.00</td>
                            <td><span class="label label-success">已平账</span></td>
                        </tr>
                        <tr>
                            <td title="华东商业保理有限公司">现金巴士</td>
                            <td>20181004</td>
                            <td>50,000.00</td>
                            <td>0</td>
                            <td>0.00</td>
                            <td>0.00</td>
                            <td>0.00</td>
                            <td><span class="label label-warning">未对账</span></td>
                        </tr>
                        <tr>
                            <td title="杭州柏阔网络科技有限公司">飞鸟贷</td>
                            <td>20181004</td>
                            <td>0.00</td>
                            <td>0</td>
                            <td>0.00</td>
                            <td>0.00</td>
                            <td>0.00</td>
                            <td><span class="label label-warning">未对账</span></td>
                        </tr>
                        <tr>
                            <td title="至弈（上海）软件有限公司">至弈</td>
                            <td>20181004</td>
                            <td>29.66</td>
                            <td>0</td>
                            <td>0.00</td>
                            <td>0.00</td>
                            <td>0.00</td>
                            <td><span class="label label-warning">未对账</span></td>
                        </tr>
                        <tr>
                            <td title="上海两橙信息科技有限公司">造艺</td>
                            <td>20181003</td>
                            <td>620,249.05</td>
                            <td>63</td>
                            <td>63,000.00</td>
                            <td>630.00</td>
                            <td>63.00</td>
                            <td><span class="label label-success">已平账</span></td>
                        </tr>
                        <tr>
                            <td title="华东商业保理有限公司">现金巴士</td>
                            <td>20181003</td>
                            <td>50,000.00</td>
                            <td>0</td>
                            <td>0.00</td>
                            <td>0.00</td>
                            <td>0.00</td>
                            <td><span class="label label-warning">未对账</span></td>
                        </tr>
                        <tr>
                            <td title="杭州柏阔网络科技有限公司">飞鸟贷</td>
                            <td>20181003</td>
                            <td>0.00</td>
                            <td>0</td>
                            <td>0.00</td>
                            <td>0.00</td>
                            <td>0.00</td>
                            <td><span class="label label-warning">未对账</span></td>
                        </tr>
                        <tr>
                            <td title="至弈（上海）软件有限公司">至弈</td>
                            <td>20181003</td>
                            <td>29.66</td>
                            <td>0</td>
                            <td>0.00</td>
                            <td>0.00</td>
                            <td>0.00</td>
                            <td><span class="label label-warning">未对账</span></td>
                        </tr>
                        <tr>
                            <td title="上海两橙信息科技有限公司">造艺</td>
                            <td>20180701</td>
                            <td>0.00</td>
                            <td>0</td>
                            <td>0.00</td>
                            <td>0.00</td>
                            <td>0.00</td>
                            <td><span class="label label-warning">未对账</span></td>
                        </tr>
                        <tr>
                            <td title="华东商业保理有限公司">现金巴士</td>
                            <td>20180701</td>
                            <td>0.00</td>
                            <td>0</td>
                            <td>0.00</td>
                            <td>0.00</td>
                            <td>0.00</td>
                            <td><span class="label label-warning">未对账</span></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="box-footer clearfix text-center">
                    <ul class="pagination">
                        <li class="disabled"><span>&laquo;</span></li>
                        <li class="active"><span>1</span></li>
                        <li><a href="/ddl_statement/statdaily.html?page=2">2</a></li>
                        <li><a href="/ddl_statement/statdaily.html?page=3">3</a></li>
                        <li><a href="/ddl_statement/statdaily.html?page=4">4</a></li>
                        <li><a href="/ddl_statement/statdaily.html?page=5">5</a></li>
                        <li><a href="/ddl_statement/statdaily.html?page=6">6</a></li>
                        <li><a href="/ddl_statement/statdaily.html?page=7">7</a></li>
                        <li><a href="/ddl_statement/statdaily.html?page=8">8</a></li>
                        <li class="disabled"><span>...</span></li>
                        <li><a href="/ddl_statement/statdaily.html?page=38">38</a></li>
                        <li><a href="/ddl_statement/statdaily.html?page=39">39</a></li>
                        <li><a href="/ddl_statement/statdaily.html?page=2">&raquo;</a></li>
                    </ul>
                </div>
            </div>
            <script src="/static/index/default/js/datepicker/WdatePicker.js"></script>
            <script type="text/javascript">
                $(function () {
                    $("#export").click(function () {
                        $('.form_single').append("<input type='hidden' name='mark' id='mark' value='export'/>");
                        $(".form_single").submit();
                    });
                    $("#search_btn").click(function () {
                        $('#mark').remove();
                        $(".form_single").submit();
                    })
                });
            </script>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!--  <footer class="main-footer">
       <div class="pull-right hidden-xs">
         <b>版本号</b> 1.0.0
       </div>
       <strong>
           版权©2014 - 2016 OneBase .
       </strong>
         保留所有权利。
     </footer> -->
    <!-- 控制栏 -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <!--    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
              <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-bell-o"></i></a></li>
              <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
            </ul>-->
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane" id="control-sidebar-home-tab">
                <!--        <h3 class="control-sidebar-heading">通知开关</h3>

                          <div class="form-group">
                            <label class="control-sidebar-subheading">
                              异常登录是否通知
                              <input type="checkbox" class="pull-right" checked>
                            </label>
                            <p>
                              不在常用地区或常用IP登录是否通知用户，默认为是。
                            </p>
                          </div>

                          <div class="form-group">
                            <label class="control-sidebar-subheading">
                              行为异常是否限制
                              <input type="checkbox" class="pull-right" checked>
                            </label>
                            <p>
                              用户行为异常是否限制其操作，默认为是。
                            </p>
                          </div>-->

            </div>

            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
                <!--        <form method="post">
                          <h3 class="control-sidebar-heading">系统开关</h3>

                          <div class="form-group">
                            <label class="control-sidebar-subheading">
                              是否允许注册
                              <input type="checkbox" class="pull-right" checked>
                            </label>
                            <p>
                              若勾选后则不允许用户注册，默认为是。
                            </p>
                          </div>

                          <div class="form-group">
                            <label class="control-sidebar-subheading">
                              是否调试模式
                              <input type="checkbox" class="pull-right" checked>
                            </label>
                            <p>
                              若为调试模式页面将显示Trace信息，默认为是。
                            </p>
                          </div>
                        </form>-->
            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<script src="/static/ext/adminlte/dist/js/app.min.js"></script>
<script src="/static/ext/adminlte/dist/js/init.js"></script>
<script src="/static/ext/toastr/toastr.min.js"></script>
<script src="/static/ext/btnloading/dist/spin.min.js"></script>
<script src="/static/ext/btnloading/dist/ladda.min.js"></script>
<script src="/static/ext/remodal/remodal.min.js"></script>
<script src="/static/admin/js/onebase.js"></script>
<script src="/static/ext/select2/js/select2.full.min.js"></script>
<link rel="stylesheet" href="/static/admin/css/ob_skin.css">
<script type="text/javascript">setTimeout(function () {
        $('.fakeloader').hide();
    }, 500);</script>
<div id="think_page_trace"
     style="position: fixed;bottom:0;right:0;font-size:14px;width:100%;z-index: 999999;color: #000;text-align:left;font-family:'微软雅黑';">
    <div id="think_page_trace_tab" style="display: none;background:white;margin:0;height: 250px;">
        <div id="think_page_trace_tab_tit"
             style="height:30px;padding: 6px 12px 0;border-bottom:1px solid #ececec;border-top:1px solid #ececec;font-size:16px">
            <span style="color:#000;padding-right:12px;height:30px;line-height:30px;display:inline-block;margin-right:3px;cursor:pointer;font-weight:700">基本</span>
            <span style="color:#000;padding-right:12px;height:30px;line-height:30px;display:inline-block;margin-right:3px;cursor:pointer;font-weight:700">文件</span>
            <span style="color:#000;padding-right:12px;height:30px;line-height:30px;display:inline-block;margin-right:3px;cursor:pointer;font-weight:700">流程</span>
            <span style="color:#000;padding-right:12px;height:30px;line-height:30px;display:inline-block;margin-right:3px;cursor:pointer;font-weight:700">错误</span>
            <span style="color:#000;padding-right:12px;height:30px;line-height:30px;display:inline-block;margin-right:3px;cursor:pointer;font-weight:700">SQL</span>
            <span style="color:#000;padding-right:12px;height:30px;line-height:30px;display:inline-block;margin-right:3px;cursor:pointer;font-weight:700">调试</span>
        </div>
        <div id="think_page_trace_tab_cont" style="overflow:auto;height:212px;padding:0;line-height: 24px">
            <div style="display:none;">
                <ol style="padding: 0; margin:0">
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">请求信息 : 2019-04-18 09:24:42
                        HTTP/1.1 GET : t01.daidailink.com/ddl_statement/statdaily.html
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">运行时间 : 0.061738s [
                        吞吐率：16.20req/s ] 内存消耗：5,492.39kb 文件加载：84
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">查询信息 : 13 queries 0 writes
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">缓存信息 : 0 reads,0 writes</li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">配置加载 : 94</li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">会话信息 :
                        SESSION_ID=7c0a0nmg1jlma1hkr61dsj28p3
                    </li>
                </ol>
            </div>
            <div style="display:none;">
                <ol style="padding: 0; margin:0">
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/public/admin.php ( 0.17 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/public/init.php ( 0.29 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/start.php ( 0.73 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/base.php ( 2.66 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/Loader.php ( 19.47 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/vendor/composer/autoload_namespaces.php ( 0.21 KB
                        )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/vendor/composer/autoload_psr4.php ( 0.53 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/vendor/composer/autoload_classmap.php ( 0.14 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/vendor/composer/autoload_files.php ( 0.33 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/vendor/topthink/think-captcha/src/helper.php (
                        1.59 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/Route.php ( 59.82 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/Config.php ( 6.03 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/Validate.php ( 40.27 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/vendor/qiniu/php-sdk/src/Qiniu/functions.php (
                        7.10 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/vendor/qiniu/php-sdk/src/Qiniu/Config.php ( 0.70
                        KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/Error.php ( 3.59 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/convention.php ( 10.31 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/App.php ( 20.63 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/Request.php ( 49.72 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/config.php ( 9.69 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/database.php ( 1.73 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/extra/logic_config.php ( 3.05 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/debug.php ( 3.02 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/Hook.php ( 4.76 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/tags.php ( 1.13 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/common/behavior/InitBase.php ( 7.38 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/common.php ( 25.56 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/Env.php ( 1.25 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/helper.php ( 17.86 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/Debug.php ( 7.06 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/Cache.php ( 6.17 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/Log.php ( 5.84 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/cache/driver/File.php (
                        6.98 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/cache/Driver.php ( 5.52 KB
                        )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/common/behavior/InitHook.php ( 1.54 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/common/model/Hook.php ( 0.28 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/common/model/ModelBase.php ( 5.38 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/Model.php ( 66.83 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/Db.php ( 6.54 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/db/connector/Mysql.php (
                        3.94 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/db/Connection.php ( 29.97
                        KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/db/Query.php ( 89.54 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/db/builder/Mysql.php ( 2.16
                        KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/db/Builder.php ( 30.47 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/common/model/Addon.php ( 0.28 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/Lang.php ( 6.95 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/lang/zh-cn.php ( 3.85 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/route.php ( 0.23 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/common/behavior/AppBegin.php ( 0.38 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/admin/config.php ( 0.68 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/admin/common.php ( 0.42 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/admin/controller/DdlStatement.php ( 6.48 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/admin/controller/DdlBase.php ( 1.98 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/admin/controller/AdminBase.php ( 4.39 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/common/controller/ControllerBase.php ( 2.75 KB
                        )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/Controller.php ( 6.20 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/traits/controller/Jump.php ( 5.44
                        KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/admin/logic/AdminBase.php ( 3.03 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/common/logic/LogicBase.php ( 0.32 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/admin/logic/Menu.php ( 8.39 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/admin/model/Menu.php ( 0.50 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/admin/model/AdminBase.php ( 0.33 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/admin/logic/AuthGroupAccess.php ( 2.19 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/View.php ( 6.86 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/view/driver/Think.php (
                        5.61 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/Template.php ( 46.46 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/template/driver/File.php (
                        2.24 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/admin/logic/DdlStatement.php ( 23.08 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/admin/logic/DdlProxy.php ( 1.28 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/Session.php ( 11.17 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/common/model/Member.php ( 0.95 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/Url.php ( 12.97 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/common/logic/Merchant.php ( 5.08 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/common/model/Merchant.php ( 1.13 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/admin/model/DdlLoanMerPay.php ( 3.86 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/admin/model/DdlBase.php ( 0.17 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/admin/model/DdlStatDaily.php ( 2.09 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/paginator/driver/Bootstrap.php
                        ( 5.43 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/Paginator.php ( 9.45 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/Collection.php ( 8.63 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/runtime/temp/0027a228dd6d4cff2604e59ddac94771.php
                        ( 21.50 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/Response.php ( 8.64 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/common/behavior/AppEnd.php ( 0.45 KB )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/library/think/debug/Html.php ( 4.27 KB )
                    </li>
                </ol>
            </div>
            <div style="display:none;">
                <ol style="padding: 0; margin:0">
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ CACHE ] INIT File</li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ BEHAVIOR ] Run
                        app\common\behavior\InitBase @app_init [ RunTime:0.001279s ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ DB ] INIT mysql</li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ BEHAVIOR ] Run
                        app\common\behavior\InitHook @app_init [ RunTime:0.009237s ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ LANG ]
                        /usr/local/nginx/html/loan2loan/code/php/loan/thinkphp/lang/zh-cn.php
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ BIND ] 'admin'</li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ ROUTE ] array (
                        'type' =&gt; 'module',
                        'module' =&gt;
                        array (
                        0 =&gt; 'admin',
                        1 =&gt; 'ddl_statement',
                        2 =&gt; 'statdaily',
                        ),
                        )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ HEADER ] array (
                        'host' =&gt; 't01.daidailink.com',
                        'connection' =&gt; 'keep-alive',
                        'upgrade-insecure-requests' =&gt; '1',
                        'user-agent' =&gt; 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko)
                        Chrome/63.0.3239.132 Safari/537.36',
                        'accept' =&gt;
                        'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                        'referer' =&gt; 'http://t01.daidailink.com/ddl_statement/statdaily.html',
                        'accept-encoding' =&gt; 'gzip, deflate',
                        'accept-language' =&gt; 'zh-CN,zh;q=0.9',
                        'cookie' =&gt; 'thinkphp_show_page_trace=0|0; PHPSESSID=7c0a0nmg1jlma1hkr61dsj28p3',
                        'content-type' =&gt; '',
                        'content-length' =&gt; '',
                        )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ PARAM ] array (
                        )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ BEHAVIOR ] Run
                        app\common\behavior\AppBegin @app_begin [ RunTime:0.000075s ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ SESSION ] INIT array (
                        'id' =&gt; '',
                        'var_session_id' =&gt; '',
                        'prefix' =&gt; 'think',
                        'type' =&gt; '',
                        'auto_start' =&gt; true,
                        )
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ RUN ]
                        app\admin\controller\DdlStatement-&gt;statdaily[
                        /usr/local/nginx/html/loan2loan/code/php/loan/app/admin/controller/DdlStatement.php ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ DB ] INIT mysql</li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ VIEW ]
                        /usr/local/nginx/html/loan2loan/code/php/loan/public/../app/admin/view/ddl_statement/statdaily_list.html
                        [ array (
                        0 =&gt; 'loading_icon',
                        1 =&gt; 'ob_title',
                        2 =&gt; 'menu_view',
                        3 =&gt; 'crumbs_view',
                        4 =&gt; 'member_info',
                        5 =&gt; 'userId',
                        6 =&gt; 'proxyId',
                        7 =&gt; 'hasShowprofitRight',
                        8 =&gt; 'income',
                        9 =&gt; 'list',
                        10 =&gt; 'merchantList',
                        11 =&gt; 'create_time1',
                        12 =&gt; 'create_time2',
                        13 =&gt; 'SUserId',
                        14 =&gt; 'check_status',
                        15 =&gt; 'merchantType',
                        ) ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ BEHAVIOR ] Run
                        app\common\behavior\AppEnd @app_end [ RunTime:0.000090s ]
                    </li>
                </ol>
            </div>
            <div style="display:none;">
                <ol style="padding: 0; margin:0">
                </ol>
            </div>
            <div style="display:none;">
                <ol style="padding: 0; margin:0">
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ DB ] CONNECT:[
                        UseTime:0.000530s ] mysql:dbname=loantest;host=127.0.0.1;port=23306;charset=utf8
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ SQL ] SHOW COLUMNS FROM
                        `yd_menu` [ RunTime:0.000879s ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ SQL ] SELECT
                        `id`,`name`,`pid`,`sort`,`module`,`url`,`is_hide`,`icon`,`status`,`update_time`,`create_time`
                        FROM `yd_menu` WHERE `status` &lt;&gt; -1 ORDER BY sort desc,id asc [ RunTime:0.000716s ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ EXPLAIN : array (
                        'id' =&gt; 1,
                        'select_type' =&gt; 'SIMPLE',
                        'table' =&gt; 'yd_menu',
                        'partitions' =&gt; NULL,
                        'type' =&gt; 'ALL',
                        'possible_keys' =&gt; NULL,
                        'key' =&gt; NULL,
                        'key_len' =&gt; NULL,
                        'ref' =&gt; NULL,
                        'rows' =&gt; 120,
                        'filtered' =&gt; 90,
                        'extra' =&gt; 'Using where; Using filesort',
                        ) ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ SQL ] SELECT
                        `id`,`name`,`pid`,`sort`,`module`,`url`,`is_hide`,`icon`,`status`,`update_time`,`create_time`
                        FROM `yd_menu` WHERE `url` = 'ddlstatement/statdaily' AND `module` = 'admin' LIMIT 1 [
                        RunTime:0.000658s ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ EXPLAIN : array (
                        'id' =&gt; 1,
                        'select_type' =&gt; 'SIMPLE',
                        'table' =&gt; 'yd_menu',
                        'partitions' =&gt; NULL,
                        'type' =&gt; 'ALL',
                        'possible_keys' =&gt; NULL,
                        'key' =&gt; NULL,
                        'key_len' =&gt; NULL,
                        'ref' =&gt; NULL,
                        'rows' =&gt; 120,
                        'filtered' =&gt; 1,
                        'extra' =&gt; 'Using where',
                        ) ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ SQL ] SELECT
                        `id`,`name`,`pid`,`sort`,`module`,`url`,`is_hide`,`icon`,`status`,`update_time`,`create_time`
                        FROM `yd_menu` WHERE `id` = 232 LIMIT 1 [ RunTime:0.000305s ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ EXPLAIN : array (
                        'id' =&gt; 1,
                        'select_type' =&gt; 'SIMPLE',
                        'table' =&gt; 'yd_menu',
                        'partitions' =&gt; NULL,
                        'type' =&gt; 'const',
                        'possible_keys' =&gt; 'PRIMARY',
                        'key' =&gt; 'PRIMARY',
                        'key_len' =&gt; '4',
                        'ref' =&gt; 'const',
                        'rows' =&gt; 1,
                        'filtered' =&gt; 100,
                        'extra' =&gt; NULL,
                        ) ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ SQL ] SELECT
                        `id`,`name`,`pid`,`sort`,`module`,`url`,`is_hide`,`icon`,`status`,`update_time`,`create_time`
                        FROM `yd_menu` WHERE `id` = 234 LIMIT 1 [ RunTime:0.000232s ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ EXPLAIN : array (
                        'id' =&gt; 1,
                        'select_type' =&gt; 'SIMPLE',
                        'table' =&gt; 'yd_menu',
                        'partitions' =&gt; NULL,
                        'type' =&gt; 'const',
                        'possible_keys' =&gt; 'PRIMARY',
                        'key' =&gt; 'PRIMARY',
                        'key_len' =&gt; '4',
                        'ref' =&gt; 'const',
                        'rows' =&gt; 1,
                        'filtered' =&gt; 100,
                        'extra' =&gt; NULL,
                        ) ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ SQL ] SELECT `name` FROM
                        `yd_menu` WHERE `module` = 'admin' AND `url` = 'ddlstatement/statdaily' LIMIT 1 [
                        RunTime:0.000277s ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ EXPLAIN : array (
                        'id' =&gt; 1,
                        'select_type' =&gt; 'SIMPLE',
                        'table' =&gt; 'yd_menu',
                        'partitions' =&gt; NULL,
                        'type' =&gt; 'ALL',
                        'possible_keys' =&gt; NULL,
                        'key' =&gt; NULL,
                        'key_len' =&gt; NULL,
                        'ref' =&gt; NULL,
                        'rows' =&gt; 120,
                        'filtered' =&gt; 1,
                        'extra' =&gt; 'Using where',
                        ) ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ SQL ] SHOW COLUMNS FROM
                        `yd_merchant` [ RunTime:0.000751s ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ SQL ] SELECT * FROM
                        `yd_merchant` [ RunTime:0.000265s ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ EXPLAIN : array (
                        'id' =&gt; 1,
                        'select_type' =&gt; 'SIMPLE',
                        'table' =&gt; 'yd_merchant',
                        'partitions' =&gt; NULL,
                        'type' =&gt; 'ALL',
                        'possible_keys' =&gt; NULL,
                        'key' =&gt; NULL,
                        'key_len' =&gt; NULL,
                        'ref' =&gt; NULL,
                        'rows' =&gt; 5,
                        'filtered' =&gt; 100,
                        'extra' =&gt; NULL,
                        ) ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ DB ] CONNECT:[
                        UseTime:0.000364s ] mysql:dbname=loantest;host=127.0.0.1;port=23306;charset=utf8
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ SQL ] SHOW COLUMNS FROM
                        `t_loanmerpay` [ RunTime:0.000756s ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ SQL ] SELECT
                        SUM(CommisionDebt) as CommisionDebt_sum,SUM(PayAgent) as PayAgent_sum,SUM(CommisionDebtCost) as
                        CommisionDebtCost_sum,SUM(PayAgentCost) as PayAgentCost_sum FROM `t_loanmerpay` WHERE `retcode`
                        IN ('TS1001','1001') LIMIT 1 [ RunTime:0.000437s ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ EXPLAIN : array (
                        'id' =&gt; 1,
                        'select_type' =&gt; 'SIMPLE',
                        'table' =&gt; 't_loanmerpay',
                        'partitions' =&gt; NULL,
                        'type' =&gt; 'ALL',
                        'possible_keys' =&gt; NULL,
                        'key' =&gt; NULL,
                        'key_len' =&gt; NULL,
                        'ref' =&gt; NULL,
                        'rows' =&gt; 71,
                        'filtered' =&gt; 20,
                        'extra' =&gt; 'Using where',
                        ) ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ SQL ] SHOW COLUMNS FROM
                        `t_stat_daily` [ RunTime:0.000612s ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ SQL ] SELECT COUNT(*) AS
                        tp_count FROM `t_stat_daily` `s` LEFT JOIN `yd_merchant` `m` ON `s`.`UserId`=`m`.`upper_shop_id`
                        LIMIT 1 [ RunTime:0.000915s ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ EXPLAIN : array (
                        'id' =&gt; 1,
                        'select_type' =&gt; 'SIMPLE',
                        'table' =&gt; 's',
                        'partitions' =&gt; NULL,
                        'type' =&gt; 'index',
                        'possible_keys' =&gt; NULL,
                        'key' =&gt; 'merchant_id',
                        'key_len' =&gt; '62',
                        'ref' =&gt; NULL,
                        'rows' =&gt; 381,
                        'filtered' =&gt; 100,
                        'extra' =&gt; 'Using index',
                        ) ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ SQL ] SELECT `s`.*,m.name
                        as merchant_name,`m`.`brand_name` FROM `t_stat_daily` `s` LEFT JOIN `yd_merchant` `m` ON
                        `s`.`UserId`=`m`.`upper_shop_id` ORDER BY s.id DESC LIMIT 0,10 [ RunTime:0.001536s ]
                    </li>
                    <li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">[ EXPLAIN : array (
                        'id' =&gt; 1,
                        'select_type' =&gt; 'SIMPLE',
                        'table' =&gt; 's',
                        'partitions' =&gt; NULL,
                        'type' =&gt; 'ALL',
                        'possible_keys' =&gt; NULL,
                        'key' =&gt; NULL,
                        'key_len' =&gt; NULL,
                        'ref' =&gt; NULL,
                        'rows' =&gt; 381,
                        'filtered' =&gt; 100,
                        'extra' =&gt; 'Using temporary; Using filesort',
                        ) ]
                    </li>
                </ol>
            </div>
            <div style="display:none;">
                <ol style="padding: 0; margin:0">
                </ol>
            </div>
        </div>
    </div>
    <div id="think_page_trace_close"
         style="display:none;text-align:right;height:15px;position:absolute;top:10px;right:12px;cursor:pointer;"><img
                style="vertical-align:top;"
                src="data:image/gif;base64,R0lGODlhDwAPAJEAAAAAAAMDA////wAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M1IFdpbmRvd3MiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MUQxMjc1MUJCQUJDMTFFMTk0OUVGRjc3QzU4RURFNkEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MUQxMjc1MUNCQUJDMTFFMTk0OUVGRjc3QzU4RURFNkEiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoxRDEyNzUxOUJBQkMxMUUxOTQ5RUZGNzdDNThFREU2QSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoxRDEyNzUxQUJBQkMxMUUxOTQ5RUZGNzdDNThFREU2QSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgH//v38+/r5+Pf29fTz8vHw7+7t7Ovq6ejn5uXk4+Lh4N/e3dzb2tnY19bV1NPS0dDPzs3My8rJyMfGxcTDwsHAv769vLu6ubi3trW0s7KxsK+urayrqqmop6alpKOioaCfnp2cm5qZmJeWlZSTkpGQj46NjIuKiYiHhoWEg4KBgH9+fXx7enl4d3Z1dHNycXBvbm1sa2ppaGdmZWRjYmFgX15dXFtaWVhXVlVUU1JRUE9OTUxLSklIR0ZFRENCQUA/Pj08Ozo5ODc2NTQzMjEwLy4tLCsqKSgnJiUkIyIhIB8eHRwbGhkYFxYVFBMSERAPDg0MCwoJCAcGBQQDAgEAACH5BAAAAAAALAAAAAAPAA8AAAIdjI6JZqotoJPR1fnsgRR3C2jZl3Ai9aWZZooV+RQAOw=="/>
    </div>
</div>
<div id="think_page_trace_open"
     style="height:30px;float:right;text-align:right;overflow:hidden;position:fixed;bottom:0;right:0;color:#000;line-height:30px;cursor:pointer;">
    <div style="background:#232323;color:#FFF;padding:0 6px;float:right;line-height:30px;font-size:14px">0.062540s</div>
    <img width="30" style="" title="ShowPageTrace"
         src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjVERDVENkZGQjkyNDExRTE5REY3RDQ5RTQ2RTRDQUJCIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjVERDVENzAwQjkyNDExRTE5REY3RDQ5RTQ2RTRDQUJCIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NURENUQ2RkRCOTI0MTFFMTlERjdENDlFNDZFNENBQkIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NURENUQ2RkVCOTI0MTFFMTlERjdENDlFNDZFNENBQkIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5fx6IRAAAMCElEQVR42sxae3BU1Rk/9+69+8xuNtkHJAFCSIAkhMgjCCJQUi0GtEIVbP8Qq9LH2No6TmfaztjO2OnUdvqHFMfOVFTqIK0vUEEeqUBARCsEeYQkEPJoEvIiELLvvc9z+p27u2F3s5tsBB1OZiebu5dzf7/v/L7f952zMM8cWIwY+Mk2ulCp92Fnq3XvnzArr2NZnYNldDp0Gw+/OEQ4+obQn5D+4Ubb22+YOGsWi/Todh8AHglKEGkEsnHBQ162511GZFgW6ZCBM9/W4H3iNSQqIe09O196dLKX7d1O39OViP/wthtkND62if/wj/DbMpph8BY/m9xy8BoBmQk+mHqZQGNy4JYRwCoRbwa8l4JXw6M+orJxpU0U6ToKy/5bQsAiTeokGKkTx46RRxxEUgrwGgF4MWNNEJCGgYTvpgnY1IJWg5RzfqLgvcIgktX0i8dmMlFA8qCQ5L0Z/WObPLUxT1i4lWSYDISoEfBYGvM+LlMQQdkLHoWRRZ8zYQI62Thswe5WTORGwNXDcGjqeOA9AF7B8rhzsxMBEoJ8oJKaqPu4hblHMCMPwl9XeNWyb8xkB/DDGYKfMAE6aFL7xesZ389JlgG3XHEMI6UPDOP6JHHu67T2pwNPI69mCP4rEaBDUAJaKc/AOuXiwH07VCS3w5+UQMAuF/WqGI+yFIwVNBwemBD4r0wgQiKoFZa00sEYTwss32lA1tPwVxtc8jQ5/gWCwmGCyUD8vRT0sHBFW4GJDvZmrJFWRY1EkrGA6ZB8/10fOZSSj0E6F+BSP7xidiIzhBmKB09lEwHPkG+UQIyEN44EBiT5vrv2uJXyPQqSqO930fxvcvwbR/+JAkD9EfASgI9EHlp6YiHO4W+cAB20SnrFqxBbNljiXf1Pl1K2S0HCWfiog3YlAD5RGwwxK6oUjTweuVigLjyB0mX410mAFnMoVK1lvvUvgt8fUJH0JVyjuvcmg4dE5mUiFtD24AZ4qBVELxXKS+pMxN43kSdzNwudJ+bQbLlmnxvPOQoCugSap1GnSRoG8KOiKbH+rIA0lEeSAg3y6eeQ6XI2nrYnrPM89bUTgI0Pdqvl50vlNbtZxDUBcLBK0kPd5jPziyLdojJIN0pq5/mdzwL4UVvVInV5ncQEPNOUxa9d0TU+CW5l+FoI0GSDKHVVSOs+0KOsZoxwOzSZNFGv0mQ9avyLCh2Hpm+70Y0YJoJVgmQv822wnDC8Miq6VjJ5IFed0QD1YiAbT+nQE8v/RMZfmgmcCRHIIu7Bmcp39oM9fqEychcA747KxQ/AEyqQonl7hATtJmnhO2XYtgcia01aSbVMenAXrIomPcLgEBA4liGBzFZAT8zBYqW6brI67wg8sFVhxBhwLwBP2+tqBQqqK7VJKGh/BRrfTr6nWL7nYBaZdBJHqrX3kPEPap56xwE/GvjJTRMADeMCdcGpGXL1Xh4ZL8BDOlWkUpegfi0CeDzeA5YITzEnddv+IXL+UYCmqIvqC9UlUC/ki9FipwVjunL3yX7dOTLeXmVMAhbsGporPfyOBTm/BJ23gTVehsvXRnSewagUfpBXF3p5pygKS7OceqTjb7h2vjr/XKm0ZofKSI2Q/J102wHzatZkJPYQ5JoKsuK+EoHJakVzubzuLQDepCKllTZi9AG0DYg9ZLxhFaZsOu7bvlmVI5oPXJMQJcHxHClSln1apFTvAimeg48u0RWFeZW4lVcjbQWZuIQK1KozZfIDO6CSQmQQXdpBaiKZyEWThVK1uEc6v7V7uK0ysduExPZx4vysDR+4SelhBYm0R6LBuR4PXts8MYMcJPsINo4YZCDLj0sgB0/vLpPXvA2Tn42Cv5rsLulGubzW0sEd3d4W/mJt2Kck+DzDMijfPLOjyrDhXSh852B+OvflqAkoyXO1cYfujtc/i3jJSAwhgfFlp20laMLOku/bC7prgqW7lCn4auE5NhcXPd3M7x70+IceSgZvNljCd9k3fLjYsPElqLR14PXQZqD2ZNkkrAB79UeJUebFQmXpf8ZcAQt2XrMQdyNUVBqZoUzAFyp3V3xi/MubUA/mCT4Fhf038PC8XplhWnCmnK/ZzyC2BSTRSqKVOuY2kB8Jia0lvvRIVoP+vVWJbYarf6p655E2/nANBMCWkgD49DA0VAMyI1OLFMYCXiU9bmzi9/y5i/vsaTpHPHidTofzLbM65vMPva9HlovgXp0AvjtaqYMfDD0/4mAsYE92pxa+9k1QgCnRVObCpojpzsKTPvayPetTEgBdwnssjuc0kOBFX+q3HwRQxdrOLAqeYRjkMk/trTSu2Z9Lik7CfF0AvjtqAhS4NHobGXUnB5DQs8hG8p/wMX1r4+8xkmyvQ50JVq72TVeXbz3HvpWaQJi57hJYTw4kGbtS+C2TigQUtZUX+X27QQq2ePBZBru/0lxTm8fOOQ5yaZOZMAV+he4FqIMB+LQB0UgMSajANX29j+vbmly8ipRvHeSQoQOkM5iFXcPQCVwDMs5RBCQmaPOyvbNd6uwvQJ183BZQG3Zc+Eiv7vQOKu8YeDmMcJlt2ckyftVeMIGLBCmdMHl/tFILYwGPjXWO3zOfSq/+om+oa7Mlh2fpSsRGLp7RAW3FUVjNHgiMhyE6zBFjM2BdkdJGO7nP1kJXWAtBuBpPIAu7f+hhu7bFXIuC5xWrf0X2xreykOsUyKkF2gwadbrXDcXrfKxR43zGcSj4t/cCgr+a1iy6EjE5GYktUCl9fwfMeylyooGF48bN2IGLTw8x7StS7sj8TF9FmPGWQhm3rRR+o9lhvjJvSYAdfDUevI1M6bnX/OwWaDMOQ8RPgKRo0eulBTdT8AW2kl8e9L7UHghHwMfLiZPNoSpx0yugpQZaFqKWqxVSM3a2pN1SAhC2jf94I7ybBI7EL5A2Wvu5ht3xsoEt4+Ay/abXgCQAxyOeDsDlTCQzy75ohcGgv9Tra9uiymRUYTLrswOLlCdfAQf7HPDQQ4ErAH5EDXB9cMxWYpjtXApRncojS0sbV/cCgHTHwGNBJy+1PQE2x56FpaVR7wfQGZ37V+V+19EiHNvR6q1fRUjqvbjbMq1/qfHxbTrE10ePY2gPFk48D2CVMTf1AF4PXvyYR9dV6Wf7H413m3xTWQvYGhQ7mfYwA5mAX+18Vue05v/8jG/fZX/IW5MKPKtjSYlt0ellxh+/BOCPAwYaeVr0QofZFxJWVWC8znG70au6llVmktsF0bfHF6k8fvZ5esZJbwHwwnjg59tXz6sL/P0NUZDuSNu1mnJ8Vab17+cy005A9wtOpp3i0bZdpJLUil00semAwN45LgEViZYe3amNye0B6A9chviSlzXVsFtyN5/1H3gaNmMpn8Fz0GpYFp6Zw615H/LpUuRQQDMCL82n5DpBSawkvzIdN2ypiT8nSLth8Pk9jnjwdFzH3W4XW6KMBfwB569NdcGX93mC16tTflcArcYUc/mFuYbV+8zY0SAjAVoNErNgWjtwumJ3wbn/HlBFYdxHvSkJJEc+Ngal9opSwyo9YlITX2C/P/+gf8sxURSLR+mcZUmeqaS9wrh6vxW5zxFCOqFi90RbDWq/YwZmnu1+a6OvdpvRqkNxxe44lyl4OobEnpKA6Uox5EfH9xzPs/HRKrTPWdIQrK1VZDU7ETiD3Obpl+8wPPCRBbkbwNtpW9AbBe5L1SMlj3tdTxk/9W47JUmqS5HU+JzYymUKXjtWVmT9RenIhgXc+nroWLyxXJhmL112OdB8GCsk4f8oZJucnvmmtR85mBn10GZ0EKSCMUSAR3ukcXd5s7LvLD3me61WkuTCpJzYAyRurMB44EdEJzTfU271lUJC03YjXJXzYOGZwN4D8eB5jlfLrdWfzGRW7icMPfiSO6Oe7s20bmhdgLX4Z23B+s3JgQESzUDiMboSzDMHFpNMwccGePauhfwjzwnI2wu9zKGgEFg80jcZ7MHllk07s1H+5yojtUQTlH4nFdLKTGwDmPbIklOb1L1zO4T6N8NCuDLFLS/C63c0eNRimZ++s5BMBHxU11jHchI9oFVUxRh/eMDzHEzGYu0Lg8gJ7oS/tFCwoic44fyUtix0n/46vP4bf+//BRgAYwDDar4ncHIAAAAASUVORK5CYII=">
</div>

<script type="text/javascript">
    (function () {
        var tab_tit = document.getElementById('think_page_trace_tab_tit').getElementsByTagName('span');
        var tab_cont = document.getElementById('think_page_trace_tab_cont').getElementsByTagName('div');
        var open = document.getElementById('think_page_trace_open');
        var close = document.getElementById('think_page_trace_close').children[0];
        var trace = document.getElementById('think_page_trace_tab');
        var cookie = document.cookie.match(/thinkphp_show_page_trace=(\d\|\d)/);
        var history = (cookie && typeof cookie[1] != 'undefined' && cookie[1].split('|')) || [0, 0];
        open.onclick = function () {
            trace.style.display = 'block';
            this.style.display = 'none';
            close.parentNode.style.display = 'block';
            history[0] = 1;
            document.cookie = 'thinkphp_show_page_trace=' + history.join('|')
        }
        close.onclick = function () {
            trace.style.display = 'none';
            this.parentNode.style.display = 'none';
            open.style.display = 'block';
            history[0] = 0;
            document.cookie = 'thinkphp_show_page_trace=' + history.join('|')
        }
        for (var i = 0; i < tab_tit.length; i++) {
            tab_tit[i].onclick = (function (i) {
                return function () {
                    for (var j = 0; j < tab_cont.length; j++) {
                        tab_cont[j].style.display = 'none';
                        tab_tit[j].style.color = '#999';
                    }
                    tab_cont[i].style.display = 'block';
                    tab_tit[i].style.color = '#000';
                    history[1] = i;
                    document.cookie = 'thinkphp_show_page_trace=' + history.join('|')
                }
            })(i)
        }
        parseInt(history[0]) && open.click();
        tab_tit[history[1]].click();
    })();
</script>
</body>
</html>