<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>...</title>
    <link rel="stylesheet" href="${re.contextPath}/plugin/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="${re.contextPath}/plugin/plugins/font-awesome/css/font-awesome.min.css" media="all"/>
    <link rel="stylesheet" href="${re.contextPath}/plugin/build/css/app.css" media="all"/>
    <link rel="stylesheet" href="${re.contextPath}/plugin/build/css/themes/default.css" media="all" id="skin" kit-skin/>
    <script type="text/javascript" src="${re.contextPath}/plugin/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="${re.contextPath}/plugin/layui/layui.all.js" charset="utf-8"></script>
    <script type="text/javascript" src="${re.contextPath}/plugin/tools/tool.js"></script>
    <script type="text/javascript" src="${re.contextPath}/plugin/tools/formatdata.js"></script>
    <style type="text/css">
        /*下拉框出现滚动条*/
        .layui-form-select dl {
            max-height: 200px;
        }

        .layui-table-cell {
            height: 50px;
            line-height: 50px;
        }
    </style>
</head>

<body layadmin-themealias="ocean-header">

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-sm6 layui-col-md3" style="margin-top: 20px;">
            <div class="layui-card" style="background-color: #27A9E3;">
                <div class="layui-card-header">
                    <span style="font-size: 16px;"><i style="font-size: 30px;" class="layui-icon layui-icon-form"></i>用户总数</span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" style="font-size: 20px;"><strong>${sum}</strong></p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3" style="margin-top: 20px;">
            <div class="layui-card" style="background-color:#5FB878;">
                <div class="layui-card-header">
                    <span style="font-size: 16px;"><i style="font-size: 30px;" class="layui-icon layui-icon-form"></i>今日新增</span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" style="font-size: 20px;"><strong>${date}</strong></p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3" style="margin-top: 20px;">
            <div class="layui-card" style="background-color:red;">
                <div class="layui-card-header">
                    <span style="font-size: 16px;"><i style="font-size: 30px;" class="layui-icon layui-icon-form"></i>绑定手机用户数</span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" style="font-size: 20px;"><strong>${mobile}</strong></p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3" style="margin-top: 20px;">
            <div class="layui-card" style="background-color:#FFB800;">
                <div class="layui-card-header">
                    <span style="font-size: 16px;"><i style="font-size: 30px;" class="layui-icon layui-icon-form"></i>专家</span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" style="font-size: 20px;"><strong>${name}</strong></p>
                </div>
            </div>
        </div>
        <#--=======================================================================================================-->
        <hr/>
        <span style="font-size: 16px;"><i style="font-size: 30px;"
                                          class="layui-icon layui-icon-user"></i>&nbsp;&nbsp;<strong>用户管理</strong></span><br/>
        <div class="lenos-search">
            <form class="layui-form">
                <div class="select">
                    &nbsp;&nbsp;&nbsp;用户名：
                    <div class="layui-inline">
                        <input class="layui-input" height="20px" id="nickName" autocomplete="off"
                               placeholder="输入关键字查询用户名"/>
                    </div>
                    真实姓名：
                    <div class="layui-inline">
                        <input class="layui-input" height="20px" id="photo" autocomplete="off" placeholder="输入专家真实姓名"/>
                    </div>
                    手机号：
                    <div class="layui-inline">
                        <input class="layui-input" height="20px" id="mobile" autocomplete="off" placeholder="输入用户手机号"/>
                    </div>
                    日期范围：
                    <div class="layui-input-inline">
                        <input type="text" class="layui-input" id="createDate" placeholder="用户加入时间范围选择"/>
                    </div>
                    用户类型：
                    <div class="layui-inline">
                        <select id="roleId">
                            <option value="0">全部</option>
                            <#if roles?exists>
                                <#list roles as role>
                                    <option value="${role.roleId}">${role.roleName}</option>
                                </#list>
                            </#if>
                        </select>
                    </div>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <button class="layui-btn layui-btn-sm layui-btn-primary" id="selUserInfo" lay-filter="select"
                            lay-submit="">
                        <i class="layui-icon">&#xe615;</i>搜索
                    </button>
                </div>
            </form>
        </div>
        <table id="userInfoList" class="layui-hide" lay-filter="userInfoList"></table>
    </div>
</div>

<style id="LAY_layadmin_theme">.layui-side-menu, .layadmin-pagetabs .layui-tab-title li:after, .layadmin-pagetabs .layui-tab-title li.layui-this:after, .layui-layer-admin .layui-layer-title, .layadmin-side-shrink .layui-side-menu .layui-nav > .layui-nav-item > .layui-nav-child {
        background-color: #344058 !important;
    }

    .layui-nav-tree .layui-this, .layui-nav-tree .layui-this > a, .layui-nav-tree .layui-nav-child dd.layui-this, .layui-nav-tree .layui-nav-child dd.layui-this a {
        background-color: #1E9FFF !important;
    }

    .layui-layout-admin .layui-logo {
        background-color: #0085E8 !important;
    }

    .layui-layout-admin .layui-header {
        background-color: #1E9FFF;
    }

    .layui-layout-admin .layui-header a, .layui-layout-admin .layui-header a cite {
        color: #f8f8f8;
    }

    .layui-layout-admin .layui-header a:hover {
        color: #fff;
    }

    .layui-layout-admin .layui-header .layui-nav .layui-nav-more {
        border-top-color: #fbfbfb;
    }

    .layui-layout-admin .layui-header .layui-nav .layui-nav-mored {
        border-color: transparent;
        border-bottom-color: #fbfbfb;
    }

    .layui-layout-admin .layui-header .layui-nav .layui-this:after, .layui-layout-admin .layui-header .layui-nav-bar {
        background-color: #fff;
        background-color: rgba(255, 255, 255, .5);
    }

    .layadmin-pagetabs .layui-tab-title li:after {
        display: none;
    }</style>
</body>
<script type="text/html" id="integralDetail">
    <lable style="color: #0000FF;"
            <@shiro.hasPermission name="userInfo:integralDetail">
                lay-event="integralDetail"
            </@shiro.hasPermission>
    ><strong>{{d.integral}}</strong></lable>
</script>
<script type="text/html" id="status">
    {{# if(d.status==1){  }}
    <a class="layui-btn layui-btn-xs layui-btn-primary">启用</a>
    {{# }else{ }}
    <a class="layui-btn layui-btn-xs layui-btn-danger">停用</a>
    {{# } }}
</script>
<script type="text/html" id="toolBar">
    <@shiro.hasPermission name="userInfo:detail">
        <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="detail"><i class="layui-icon">&#xe615;</i>详情</a>
    </@shiro.hasPermission>
    <@shiro.hasPermission name="userInfo:updateMobile">
        {{# if(d.mobile!=undefined && d.mobile!=''){  }}
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="updateMobile"><i
                    class="layui-icon">&#xe64d;</i>解绑手机号</a>
        {{# } }}
    </@shiro.hasPermission>
</script>


<script>
    layui.use(['element', 'form', 'layer', 'table', 'laydate'], function () {
        var element = layui.element,
            form = layui.form,
            $ = layui.$,
            laydate = layui.laydate,
            layer = layui.layer,
            table = layui.table;

        form.render("select");
        //日期范围
        laydate.render({
            elem: '#createDate'
            , range: true
        });
        document.onkeydown = function (e) { // 回车提交表单
            var theEvent = window.event || e;
            var code = theEvent.keyCode || theEvent.which;
            if (code == 13) {
                queryReloadTable();
            }
        };

        //方法级渲染
        table.render({
            id: 'userInfoList',
            elem: '#userInfoList'
            , url: 'showUserInfoList'
            , cols: [[
                {
                    field: 'photo',
                    title: '头像',
                    align: 'center',
                    templet: "<div><img src='{{d.photo}}' alt='头像' height='44px;' width='50px;' class='layui-upload-img layui-circle' /></div>"
                }
                , {field: 'nickName', title: '用户名', align: 'center'}
                , {field: 'realName', title: '真实姓名', align: 'center'}
                , {
                    field: 'mobile', title: '手机号', align: 'center', templet: function (date) {
                        if (date.mobile == undefined || date.mobile == '') {
                            return "暂无绑定手机号";
                        } else {
                            var mobileStr = date.mobile + "";
                            return mobileStr.substr(0, 3) + "****" + mobileStr.substr(mobileStr.length - 4, mobileStr.length);
                        }
                    }
                }
                , {
                    field: 'createDate',
                    title: '加入时间',
                    templet: "<div>{{layui.laytpl.toDateString(d.createDate,'yyyy-MM-dd HH:mm:ss')}}</div>",
                    align: 'center'
                }
                , {
                    field: 'integral', title: '当前积分数', align: 'center', sort: true, templet: '#integralDetail'
                }
                , {field: 'status', title: '状态', templet: '#status', align: 'center'}
                , {field: 'dataOp', title: '操作', toolbar: "#toolBar", width: 240, align: 'center'}
            ]]
            , page: true
            , height: 'full-83'
        });

        form.on('submit(select)', function (data) {
            queryReloadTable();
            return false;
        });

        //条件查询|利用layui的重载
        function queryReloadTable() {
            var mobile = $('#mobile').val();
            if ("" != mobile) {
                if (isNaN(mobile)) {
                    return false;
                }
            }
            var dateStr = $('#createDate').val();
            var createDate = null;
            var updateDate = null;
            if ("" != dateStr) {
                createDate = dateStr.substr(0, dateStr.lastIndexOf(" - ")).trim();
                updateDate = dateStr.substr(dateStr.lastIndexOf(" - ") + 3).trim();
            }
            table.reload('userInfoList', {
                url: 'showUserInfoList'
                , where: {
                    nickName: $('#nickName').val(),
                    photo: $('#photo').val(),//realName 专家真实姓名
                    mobile: mobile,
                    createDateStr: createDate == null ? $('#createDate').val() : createDate,
                    updateDateStr: updateDate == null ? $('#createDate').val() : updateDate,
                    introduce: $('#roleId').val() == 0 ? '' : $('#roleId').val()
                } //设定异步数据接口的额外参数
            });
        }

        //监听工具条
        table.on('tool(userInfoList)', function (obj) {
            var data = obj.data;
            if (obj.event === 'detail') {
                goDoThings('用户信息', 'goDoUserInfo?userId=' + data.userId);
            } else if (obj.event === 'updateMobile') {
                layer.confirm('确定解绑?', function () {
                    var btn_sub = layer.load(1, {
                        shade: [0.5, '#000'] //0.1透明度的背景
                    });
                    $.ajax({
                        url: "updateMobile",
                        type: 'post',
                        data: {userId: data.userId},
                        traditional: true,
                        success: function (d) {
                            layer.close(btn_sub);
                            if (d.flag) {
                                layer.msg(d.msg, {
                                    icon: 1, time: 1000 //2秒关闭（如果不配置，默认是3秒）
                                }, function () {
                                    queryReloadTable();
                                });
                            } else {
                                layer.msg(d.msg, {icon: 5});
                            }
                        }, error: function (e) {
                            layer.alert("发生错误", {icon: 6}, function () {
                                layer.close(btn_sub);
                                var index = parent.layer.getFrameIndex(window.name);
                                parent.layer.close(index);
                            });
                        }
                    });
                });
            } else if (obj.event == 'integralDetail') {
                layer.open({
                    id: 'userInfo-integralDetail',
                    type: 2,
                    offset: 't',
                    area: [600 + 'px', '100%'],
                    fix: false,
                    shadeClose: false,
                    scrollbar: false,
                    shade: 0.4,
                    title: "用户积分明细",
                    content: "toUserIntegralDetail?userId=" + data.userId
                });
            }
        });

        //添加商户信息
        function goDoThings(title, url) {
            if (title == null || title == '') {
                title = false;
            }
            if (url == null || url == '') {
                url = "/error/404";
            }
            layer.open({
                id: 'userInfo-doThings',
                type: 2,
                offset: 't',
                area: ['100%', '100%'],
                fix: false,
                shadeClose: false,
                scrollbar: false,
                shade: 0.4,
                title: title,
                content: url
            });
        }

    });


</script>

</html>
