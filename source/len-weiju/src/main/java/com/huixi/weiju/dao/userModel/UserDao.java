package com.huixi.weiju.dao.userModel;

import com.huixi.weiju.pojo.userModel.DO.UserDO;
import java.util.List;

/**
  * mybatis 对应生成的方法接口
  *@author: 李辉
  *@date: 2019/11/18 22:53
  *@param:
  *@return:
  */
public interface UserDao {
    int deleteByPrimaryKey(String userId);

    int insert(UserDO record);

    /**
     *  为每个值加上判断
     * @Author 李辉
     * @Date 2019/11/24 18:23
     * @param userDO
     * @return int
     **/
    int insertCondition(UserDO userDO);


    UserDO selectByPrimaryKey(String userId);

    List<UserDO> selectAll();

    int updateByPrimaryKey(UserDO record);
}