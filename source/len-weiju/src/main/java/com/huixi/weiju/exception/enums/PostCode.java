package com.huixi.weiju.exception.enums;

/**
 *  跟帖子有关的 状态码
 * @Author 李辉 
 * @Date 2019/9/18 15:35
 * @param 
 * @return 
 **/
public enum  PostCode implements IErrorCode {

    /**
     * POST 100 error code enum.
     */
    POST100(100, "参数异常"),
    /**
     * POST 401 error code enum.
     */
    POST401(401, "无访问权限"),
    /**
     * POST 500 error code enum.
     */
    POST500(500, "未知异常"),
    /**
     * POST 403 error code enum.
     */
    POST403(403, "无权访问"),
    /**
     * POST 404 error code enum.
     */
    POST404(404, "找不到指定资源"),
    
    ;


    private long code;
    private String message;

    PostCode(long code, String message){
        this.code = code;
        this.message=  message;
    }
    
    
    @Override
    public long getCode() {
        return 0;
    }

    @Override
    public String getMessage() {
        return null;
    }
}
