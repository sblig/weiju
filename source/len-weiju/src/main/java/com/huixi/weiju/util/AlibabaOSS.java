package com.huixi.weiju.util;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;


/**
 * 阿里巴巴OSS存储库,对上传下载删除修改的一个封装
 * 注意：用的时候必须使用@AutoWrite ，不然不会生效
 *
 * @author: 李辉
 * @date: 2019/9/8 1:01
 * @param:
 * @return:
 */
@Configuration
@Slf4j
public class AlibabaOSS {


    /**
      * 外网访问
      *@author: 李辉
      *@date: 2019/10/27 17:02
      */
    @Value("${alibabaOSS.endpoint}")
    private String endpoint;

    /**
      * 内网访问
      *@author: 李辉
      *@date: 2019/10/27 17:03
      */
    @Value("${alibabaOSS.endpointIntranet}")
    private String endpointIntranet;

    @Value("${alibabaOSS.accessKeyId}")
    private String accessKeyId;

    @Value("${alibabaOSS.accessKeySecret}")
    private String accessKeySecret;

    @Value("${alibabaOSS.bucketName}")
    private String bucketName;


    private OSS ossClient;

    private OSS ossIntranetClient;

    /**
      * 操作外网
      *@author: 李辉
      *@date: 2019/10/27 16:34
      *@param:
      *@return:
      */
    public OSS getOssClient() {

        // 创建OSSClient实例。官方封装了对操作文件的方法
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        return ossClient;

    }

    /**
      * 操作内网
      *@author: 李辉
      *@date: 2019/10/27 16:34
      *@param:
      *@return:
      */
    public OSS getOssIntranetClient() {

        // 创建OSSClient实例。官方封装了对操作文件的方法
        OSS ossClient = new OSSClientBuilder().build(endpointIntranet, accessKeyId, accessKeySecret);
        return ossClient;

    }


    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getEndpointIntranet() {
        return endpointIntranet;
    }

    public void setEndpointIntranet(String endpointIntranet) {
        this.endpointIntranet = endpointIntranet;
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getAccessKeySecret() {
        return accessKeySecret;
    }

    public void setAccessKeySecret(String accessKeySecret) {
        this.accessKeySecret = accessKeySecret;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }
}
