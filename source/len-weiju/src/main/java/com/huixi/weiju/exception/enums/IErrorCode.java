package com.huixi.weiju.exception.enums;

/**
 * @author libe
 */
public interface IErrorCode {

    long getCode();

    String getMessage();
}
