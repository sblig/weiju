package com.huixi.weiju.dao;

import com.huixi.weiju.pojo.test;
import java.util.List;

public interface testMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(test record);

    test selectByPrimaryKey(Integer id);

    List<test> selectAll();

    int updateByPrimaryKey(test record);
}