package com.huixi.weiju.controller.user;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.alibaba.fastjson.JSON;
import com.huixi.weiju.exception.enums.UserCode;
import com.huixi.weiju.exception.util.CommonResult;
import com.huixi.weiju.pojo.userModel.DO.UserDO;
import com.huixi.weiju.pojo.userModel.DTO.WxSesssionKeyDTO;
import com.huixi.weiju.pojo.userModel.VO.EncryptionUserInfoVO;
import com.huixi.weiju.service.userModel.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;


/**
  * 微距用户 调用接口（如果叫UserController 的话 与 com.len.sys 中有一模一样的 bean, spring 会报错）
  *@author: 李辉
  *@date: 2019/11/21 0:35
  *@param:
  *@return:
  */
@CrossOrigin
@RestController
@RequestMapping("/user")
@Slf4j
@Api(tags = "用户模块的接口")
public class WjUserController {

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    UserService userService;


    /**
      * 用于授权接口； 获取用户的code，获取其openID 和 个人的简单信息
     *  通过调用接口（如 wx.getUserInfo）获取数据时，接口会同时返回 rawData、signature，其中 signature = sha1( rawData + session_key )
     * 开发者将 signature、rawData 发送到开发者服务器进行校验。服务器利用用户对应的 session_key 使用相同的算法计算出签名 signature2 ，比对 signature 与 signature2 即可校验数据的完整性。
     *
      *@author: 李辉
      *@date: 2019/11/20 23:32
      *@return:
      */
    @PostMapping("/userAuthorization")
    @ApiOperation(value = "用于授权接口； 获取用户的code，获取其openID 和 个人的简单信息")
    public CommonResult userAuthorization(@RequestBody EncryptionUserInfoVO encryptionUserInfoVO) {

        String object = redisTemplate.opsForValue().get(encryptionUserInfoVO.getSessionKeyToUuid()).toString();
        WxSesssionKeyDTO wxSesssionKeyDTO = JSON.parseObject(object, WxSesssionKeyDTO.class);

        String sha1 =
                SecureUtil.sha1(encryptionUserInfoVO.getRawData() + wxSesssionKeyDTO.getSession_key());

        // 参数校验不一致
        if(!sha1.equals(encryptionUserInfoVO.getSignature())) {
            return CommonResult.failed(UserCode.USER003);
        }

        // 返回加密的数据
        UserDO encryptionUserInfo = userService.getEncryptionUserInfo(encryptionUserInfoVO.getEncryptedData(), wxSesssionKeyDTO.getSession_key(),
                encryptionUserInfoVO.getIv());


        return CommonResult.success(encryptionUserInfo);
    }





    /**
      * 获取用户的code，拿到微信后台去换取 session_key，存到redis 中，并返回UUID 给前端。
      *@author: 李辉
      *@date: 2019/11/21 0:00
      *@param code 用户code
      *@return:
      */
    @GetMapping("getUserCode/{code}")
    @ApiOperation(value = "获取用户的code，拿到微信后台去换取 session_key")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "用户的code", required = true, dataType = "String")
    })
    public CommonResult getUserCode(@PathVariable("code") String code) {


        // 用户 code 为空
        if(StrUtil.isEmpty(code)) {
            log.error(UserCode.USER001.getMessage());
            return CommonResult.failed(UserCode.USER001);
        }


        String wxSessionKey = null;
        try {
            wxSessionKey = userService.getWxSessionKey(code);
        } catch (Exception e) {
            // session_key 获取失败
            log.error(UserCode.USER002.getMessage() + "|" + e.getMessage());
            return CommonResult.failed(UserCode.USER002);
        }


        return CommonResult.success(wxSessionKey);
    }


    /**
     *  检测用户登录情况 授权过就直接返还信息给他 | 反之就给null
     * @Author 李辉
     * @Date 2019/11/24 21:04
     * @param code
     * @return com.huixi.weiju.exception.util.CommonResult
     **/
    @PostMapping("/detectionUserAuthorization")
    @ApiOperation(value = "获取用户的code，拿到微信后台去换取 session_key")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "用户的code", required = true, dataType = "String")
    })
    public CommonResult detectionUserAuthorization(@RequestBody String code) throws Exception {

        String code1 = (String) JSON.parseObject(code).get("code");
        System.out.println(code1);

        UserDO userDO = userService.detectionUserAuthorization(code1);

        return CommonResult.success(userDO);



    }


}
