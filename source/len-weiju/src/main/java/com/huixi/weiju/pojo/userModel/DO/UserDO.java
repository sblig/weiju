package com.huixi.weiju.pojo.userModel.DO;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户主表
 *
 * @author: 李辉
 * @date: 2019/11/18 22:53
 * @param:
 * @return:
 */
@Data
public class UserDO implements Serializable {
    private String userId;

    private String nickName;

    private String realName;

    private String headPortrait;

    private Integer sex;

    private String mobile;

    private String company;

    private String email;

    private String signature;

    private String address;

    private String password;

    private String identityCard;

    private Date createDate;

    private String createBy;

    private Date updateDate;

    private String updateBy;

    private Integer status;

    private String introduce;


}