package com.huixi.weiju.exception.enums;

/**
 * 枚举了一些常用API操作码  通用类的状态码(字典类有关，设置有关，页面素材有关)
 * 格式：模块名+数字+01 02... 比如USER999901 USER999902
 * @author libe
 */
public enum ResultCode implements IErrorCode {

    /** 操作成功 */
    SUCCESS(200,"操作成功"),
    /** 操作失败 */
    FAILED(500,"操作失败"),
    /** 参数检验失败 */
    VALIDATE_FAILED(404,"参数检验失败"),
    /** token过期或未登录 */
    UNAUTHORIZED(401,"暂未登录或token已经过期"),
    /** 没有权限 */
    FORBIDDEN(403,"没有相关权限"),







    /**
     * COMMONLY 100 error code enum.
     */
    COMMONLY100(100, "参数异常"),
    /**
     * COMMONLY 401 error code enum.
     */
    COMMONLY401(401, "无访问权限"),
    /**
     * COMMONLY 500 error code enum.
     */
    COMMONLY500(500, "未知异常"),
    /**
     * COMMONLY 403 error code enum.
     */
    COMMONLY403(403, "无权访问"),
    /**
     * COMMONLY 404 error code enum.
     */
    COMMONLY404(404, "找不到指定资源"),

    /**
     *PAGEMATERIAL 100 error code enum.
     */
    PAGEMATERIAL100(100, "参数异常"),
    /**
     *PAGEMATERIAL 401 error code enum.
     */
    PAGEMATERIAL401(401, "无访问权限"),
    /**
     *PAGEMATERIAL 500 error code enum.
     */
    PAGEMATERIAL500(500, "未知异常"),
    /**
     *PAGEMATERIAL 403 error code enum.
     */
    PAGEMATERIAL403(403, "无权访问"),
    /**
     *PAGEMATERIAL 404 error code enum.
     */
    PAGEMATERIAL404(404, "找不到指定资源"),



    REDIS001(001, "redis存储失败");



    private long code;
    private String message;

    ResultCode(long code, String message){
        this.code = code;
        this.message=  message;
    }

    @Override
    public long getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
