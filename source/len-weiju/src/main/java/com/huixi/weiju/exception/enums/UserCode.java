package com.huixi.weiju.exception.enums;

/**
 *  跟用户有关联的错误状态码信息
 * @Author 李辉 
 * @Date 2019/9/18 15:34
 * @param 
 * @return 
 **/
public enum UserCode implements IErrorCode{


    /**
     * USER 100 error code enum.
     */
    USER100(100, "参数异常"),
    /**
     * USER 401 error code enum.
     */
    USER401(401, "无访问权限"),
    /**
     * USER 500 error code enum.
     */
    USER500(500, "未知异常"),
    /**
     * USER 403 error code enum.
     */
    USER403(403, "无权访问"),
    /**
     * USER 404 error code enum.
     */
    USER404(404, "找不到指定资源"),

    USER001(001, "用户code为空"),

    USER002(002, "用户session_key 获取失败"),

    USER003(003, "用户信息校验不一致"),

    USER004(004, "用户加密信息解密失败");


    private long code;
    private String message;

    UserCode(long code, String message){
        this.code = code;
        this.message=  message;
    }

    @Override
    public long getCode() {
        return 0;
    }

    @Override
    public String getMessage() {
        return null;
    }
}
