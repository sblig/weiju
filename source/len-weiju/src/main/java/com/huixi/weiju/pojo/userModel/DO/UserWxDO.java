package com.huixi.weiju.pojo.userModel.DO;

import lombok.Data;

import java.io.Serializable;

/**
 *  用来存储微信官方的一些特殊值
 * @Author 李辉 
 * @Date 2019/11/22 0:54
 * @param 
 * @return 
 **/
@Data
public class UserWxDO implements Serializable {
    private String userId;

    private String wxOpenId;

    private String wxUnionId;

    private String wxAppId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getWxOpenId() {
        return wxOpenId;
    }

    public void setWxOpenId(String wxOpenId) {
        this.wxOpenId = wxOpenId == null ? null : wxOpenId.trim();
    }

    public String getWxUnionId() {
        return wxUnionId;
    }

    public void setWxUnionId(String wxUnionId) {
        this.wxUnionId = wxUnionId == null ? null : wxUnionId.trim();
    }

    public String getWxAppId() {
        return wxAppId;
    }

    public void setWxAppId(String wxAppId) {
        this.wxAppId = wxAppId == null ? null : wxAppId.trim();
    }
}