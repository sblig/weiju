package com.len.util;

public class ApproveUtil {


    public static String judgeProcessInformation(Object entity, String processInstanceId, String taskId) {
        String errorMsg = "";
        if (entity == null) {
            errorMsg = "<font style='color:red'>获取数据失败</font>";
        } else if (processInstanceId == null || processInstanceId == ""
                || taskId == null || taskId == "") {
            errorMsg = "<font style='color:red'>获取流程信息失败</font>";
        }
        return errorMsg;
    }
}
